plugins {
    kotlin("jvm") version "1.5.10"
}

group = "nb.util"
version = "1.0-0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
}
