package nb.util.arguments

class ArgumentNotFound(val argumentKey: ArgumentKey<*>): Exception("Argument was not found for key: ${argumentKey.keyName}")

object ArgumentsLocked: Exception("Cannot add an argument for a locked Arguments")

