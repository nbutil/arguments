package nb.util.arguments

interface ArgumentKey<T> {
    val keyName: String

    operator fun invoke(value: T) = this to value
}

data class ArgumentKeyImpl<T>(
    override val keyName: String
): ArgumentKey<T>

fun <T> ArgumentKey(keyName: String): ArgumentKey<T> = ArgumentKeyImpl(keyName)
