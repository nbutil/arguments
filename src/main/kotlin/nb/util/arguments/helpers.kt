package nb.util.arguments

operator fun <K, V> MutableMap<K, V>.set(key: K, overwrite: Boolean, value: V) {
    if (!contains(key) || overwrite) {
        this[key] = value
    }
}
