package nb.util.arguments

class Arguments private constructor(
    val arguments: MutableMap<ArgumentKey<*>, Any?>,
    val lockKeys: Boolean
): Iterable<Map.Entry<ArgumentKey<*>, *>> {
    constructor(argMap: Map<ArgumentKey<*>, Any?>): this(if (argMap is MutableMap) argMap else argMap.toMutableMap(), false)
    constructor(vararg arguments: Pair<ArgumentKey<*>, Any?>): this(arguments.toMap().toMutableMap(), false)

    companion object {
        val EMPTY get() = Arguments(mutableMapOf(), true)
    }

    operator fun <T> get(key: ArgumentKey<T>): T? {
        if (key in this.arguments) {
            return this.arguments[key] as T
        }
        return null
        throw ArgumentNotFound(key)
    }

    operator fun <T> get(key: ArgumentKey<T>, default: T): T = if (key in arguments) {
        this.arguments[key] as T
    } else {
        default
    }

    operator fun <T> get(key: ArgumentKey<T>, compute: () -> T): T = if (key in arguments) {
        this.arguments[key] as T
    } else {
        compute()
    }

    fun <T> putIfAbsent(key: ArgumentKey<T>, value: T) {
        if (key !in this.arguments) {
            this.arguments[key] = value
        }
    }

    fun <T> computeIfAbsent(key: ArgumentKey<T>, value: () -> T) {
        if (key !in this.arguments) {
            putIfAbsent(key, value())
        }
    }

    fun <T> getOrDefault(key: ArgumentKey<T>, default: T) = (this.arguments[key] ?: default) as T

    fun <T> getOrCompute(key: ArgumentKey<T>, default: () -> T) = (this.arguments[key] ?: default()) as T

    operator fun <T> contains(key: ArgumentKey<T>): Boolean = key in this.arguments

    operator fun <T> set(key: ArgumentKey<T>, overwrite: Boolean = true, value: T) {
        if (this.lockKeys) {
            throw ArgumentsLocked
        }
        if (key !in this || overwrite) {
            this.arguments[key] = value
        }
    }

    fun setRaw(key: ArgumentKey<*>, overwrite: Boolean = true, value: Any?) {
        if (key !in this || overwrite) {
            this.arguments[key] = value
        }
    }

    fun remove(key: ArgumentKey<*>) {
        this.arguments.remove(key)
    }

    fun copyOf(vararg additionalArguments: Pair<ArgumentKey<*>, Any?>): Arguments = Arguments(
        arguments = this.arguments.apply { additionalArguments.forEach { set(it.first, it.second) } },
        lockKeys = this.lockKeys
    )

    fun mergeBy(otherArguments: Arguments, overwrite: Boolean = false) {
        otherArguments.forEach { (key, value) ->
            this.setRaw(key, overwrite, value)
        }
    }

    override fun iterator() = arguments.iterator()

    override fun toString(): String = buildString {
        append("[")
        arguments.forEach {
            append("(${it.key.keyName}=${it.value}),")
        }
        append("]")
    }
}
