package nb.util.arguments

class ArgumentContext(
    private val arguments: Arguments
) {
    fun <T> get(key: ArgumentKey<T>) = arguments[key]
    fun <T> getNotNull(key: ArgumentKey<T>) = get(key)!!
    fun <T> getOrDefault(key: ArgumentKey<T>, default: T) = arguments.getOrDefault(key, default)
}
